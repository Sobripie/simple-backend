## Simple backend server (deployed on heroku)

Main purpose is to reroute requests to API if an API key is needed.
Made with Express.

### How to

#### Dev

- Clone, cd to directory

- Create an .env file
```
touch .env
```

- Copy contents from .env_example to .env
```
PORT={your_port}
GOOGLE_API_KEY={your_api_key}
```

- You can now start the server on the local machine, it will listen at the port you've provided.
```
npm run start:dev
```

#### Production on deployment on heroku

- Add everything, commit changes, login in heroku CLI, push changes

- .env variables are set like this
```
heroku config:set ENV_VAR_NAME=value
```