import * as dotenv from "dotenv";
import express, { Request } from "express";
import cors from "cors";
import helmet from "helmet";
import fetch from "node-fetch";

dotenv.config();
const PORT: number = process.env.PORT ? parseInt(process.env.PORT, 10) : 10124;
const GOOGLE_API_KEY = process.env.GOOGLE_API_KEY;
if (!GOOGLE_API_KEY) {
  console.error("No Google API key was provided.");
  process.exit(1);
}

const app = express();
app.use(helmet());
app.use(cors());
app.use(express.json());

app.set("query parser", "simple");

app.get("/api/booksearcher/full/:id", async (inc, out) => {
  const resultUrl = `https://www.googleapis.com/books/v1/volumes/${inc.params.id}?key=${GOOGLE_API_KEY}`;
  const response = await fetch(resultUrl);
  const body = await response.json();
  out.status(response.status).send(body);
});

/** Пришлось вручную оверрайдить тип реквеста, чтобы query был не `QueryString.ParsedQs`
 * Почему вот это не работает, я не знаю
 * ```ts
 * interface CustomRequest extends Omit<Request, "query"> {
 *  query: string;
 * }
 * ```
 */
app.get("/api/booksearcher/volumes", async (inc: Request<{}, any, any, string, Record<string, any>>, out) => {
  const query = new URLSearchParams(inc.query);
  query.append("key", GOOGLE_API_KEY);
  const response = await fetch(`https://www.googleapis.com/books/v1/volumes?${query.toString()}`);
  const body = await response.json();
  out.status(response.status).send(body);
});

app.listen(PORT, () => {
  console.log(`Listening on port ${PORT}`);
});